all:
	make pdf
	bibtex icisa
	make pdf
	make pdf
pdf:
	xelatex icisa.tex
clean:
	rm -f *.bbl
	rm -f *.aux
	rm -f *.blg
	rm -f *.log
	rm -f *.out
	#rm -f *.pdf
	rm -f *.toc
