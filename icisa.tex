% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
\usepackage{float}
\usepackage[caption=false]{subfig}
\usepackage[nocompress]{cite}

% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}
\graphicspath{{fig/}}

\begin{document}
%
\title{Disguised Face Detection}
%
%\titlerunning{Abbreviated paper title}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Arsenii Zorin\inst{1}\orcidID{0000-0001-7858-4040} \and
Nikolay Abramov\inst{1}\orcidID{0000-0002-4252-2603}}
%
% \authorrunning{F. Author et al.}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{Peter the Great St.Petersburg Polytechnic University, 
Russia, 195251, St.Petersburg, Polytechnicheskaya, 29\\
\email{\{arseny.zorin, nikolay.abramov\}@spbpu.com}}
%
\maketitle              % typeset the header of the contribution
%
\begin{abstract}
Research in face detection has seen tremendous growth over the past couple of
decades.
Beginning from algorithms capable of performing detection in constrained 
environments, the current face detection systems achieve very high accuracies
on large-scale unconstrained face datasets.
While upcoming algorithms continue to achieve improved performance, a majority
of the face detection systems are susceptible to failure under disguise
variations, one of the most challenging covariate of face detection.
In this article, we propose a method for disguised face detection.
The method consists of three stages.
The first stage detects a face.
The second one finds parts of the face on an output from the first stage.
The third stage determines whether the face is disguised.
Experiments show that our method can detect disguised faces in real time under
the complex background and achieve acceptable disguised face detection rate.

\keywords{Disguised face detection \and Convolutional neural networks \and Face parts
\and Computer vision \and Neural networks \and Machine learning \and Face detection.}
\end{abstract}

\section{Introduction}

Face detection is a tremendously important field in computer vision needed for face
recognition, sentiment analysis, video surveillance and many other fields.
Given an arbitrary image, the goal of face detection is to determine whether there
are any faces in the image, return the image location and extent of each face.

Numerous algorithms and techniques have been developed for face detection.
These systems are usually susceptible to a number of challenges including illumination,
image quality, expression, pose, disguise etc.
Among these challenges, detection of faces with disguise is a major challenge.
So far, most of the researchers of disguised faces mainly focus on face recognition
with disguises, while there is hardly any research on disguised face detection.

Disguise is an important and crucial face detection problem.
Nowadays it has become very difficult to process a huge amount of information from
video surveillance systems in public places such as ATM rooms, subways, stadium
entrances and so on.
The goal of our approach is to detect disguised faces on a videostream from CCTV and
notify operator in case there is a detected one.

The reminder of this paper is organized as follows.
Section 2 presents the research background. 
Section 3 describes the proposed approach.
Section 4 presents the experimental results.
Finally, Section 5 concludes this paper.

\section{Related Work}

Face detection has been extensively studied from its emergence in 1990s to the present
due to its wide practical applications.
There were a lot of research in that area.
For example, Saumya Kumaar et al.~\cite{keypointrecogn} proposed an approach for
face verification based on facial key-points prediction.
It consists of two neural networks, first one being a convolutional neural network
that predicts 20 facial key-points in the image and the second neural network
classifies the subject based on the angles and ratios calculated from the predicted
points.
Prediction of facial key-points has achieved state of the art performance on face 
recognition and verification tasks.
This approach could be suitable in ordinary face detection tasks, but we could not
use it in our case due to prediction of facial key-points. 
They are always detected or predicted on a face and it is not possible to determine
whether the face is disguised or not.

Jing Li et al.~\cite{complexbackground} proposed a method for disguised face 
detection and recognition under the complex background.
This method consists of two stages.
The first stage determines whether the object is a person.
In this stage, they proposed the first-dynamic-then-static foreground object
detection strategy.
This strategy exploits the updated learning-based codebook model for moving 
object detection and uses the Local Binary Patterns (LBP) + Histogram of Oriented
Gradients (HOG) feature-based head-shoulder detection for static target detection.
The second stage determined whether the face is disguised and the classes of disguises. 
This approach trains Adaboost classifier with certain types of disguises, e.g.
sunglasses, caps and medicine masks, which are limited samples of disguise.

Abhila A.G. and Sreeletha S.H.~\cite{irjet} used Viola Jones method for
disguised face detection. The used algorithm works by finding certain haar-like
features. These features are used to determine whether there is a face on image
or not.
This algorithm detects face on an image regardless of whether is is disguised or
not.
Purpose of this paper is to identify disguised face while our purpose is to
detect face on a frame and decide whether it is disguised or not.

Tamgale S.B. and Kothawade S.N.~\cite{tamgale} used a Deep Convolutional
Neural Network architecture to identify users with disguised face attempting
a fraudulent ATM transaction.
The network was trained using a dataset of images captures in similar situations
as if on ATM. 
There were three classes:

\begin{itemize}
\item Disguised face
\item Partially Disguised face
\item Undisguised face
\end{itemize}

This approach demonstrates high accuracy in classification only with low
background clutter on the frames.
In other case there are a lot of false positives on background.
Objects on background that looked like face or had shape like face could be 
classified as disguised face.

Our task is to detect a face and to make a conclusion whether it is disguised
or not.
We do not have to make attempt to recognize detected face.
All reviewed methods from the above are not suitable for our case.
Thus, we are proposing an experimental method for the task of disguised face
detection, which accumulates different methods and approaches.

\section{Proposed Approach}

The first step in the proposed approach is an attempt to detect every face
on a frame from the videostream.
On this step it is not important for us the face is disguised or no.
The second step is an attempt to detect every visible 
face part (eyes, nose, mouth) on the face from the first neural network.
The smaller the face found at the first step, the more complicated the task
to detect face parts on it.
To reduce this complication we decided to limit the smallest face size
to be detected.

As Convolutional Neural Networks (CNNs) are widely used in image and video processing,
we decided to use them for detection.
Thus, in the proposed approach, we use two connected convolutional neural networks.
The second one works on the detected are from the first one.

The most important part in our task is to determine whether the detected face
is disguised or not.
This is the tricky part because of the difference in views on what to consider 
as a disguised face.
Some think that it is enough to have sunglasses or hide your eyes to disguise
the face, some think that you have to hide your mouth with a medicine mask.
In our approach we decided that it is more than that.
We are making our conclusion based on detected amount of face parts.
In case of absence of certain face parts combination, we make a conclusion about face
disguising.
For example, if we could not find mouth and nose on the same face, we make
a conclusion that it is a disguised face.

Before all computations and detections, a frame has to be prepared for further
computations.
To prepare the frame obtained from a videostream we convert it from 
RGB (red, green and blue light) format to grayscale format.

In our task color on a frame has no significance and this step allows us to 
simplify all computations on account of frame dimension reducing
(from 3 dimensional to 1 dimensional).

\subsection{Network Design}

Our approach is based on two convolutional neural networks evaluated on our custom
datasets. 
The initial convolutional layers of the networks extract features from the frame while
the fully connected layers predict the output probabilities and coordinates.

Our networks architectures are inspired by the Fast YOLO model~\cite{darknet13}.
Our networks have 9 and 8 layers for face detection and face parts detection 
respectively followed by 2 fully connected layers.
We use $1\times1$ reduction layers followed by $3\times3$ convolutional layers.
The main difference in our networks is amount of layers, thus we have only one
figure that shows network architecture(Fig. ~\ref{network}).

\begin{figure}[h!]
\includegraphics[width=\textwidth]{networkdiag.eps}
\caption{Network architecture.} 
\label{network}
\end{figure}

The final output of our network is the $7\times7\times30$ tensor of predictions.

\subsection{Dataset}

Recently, researches have proposed large scale datasets captured in uncontrolled
scenarios for performing face detection
\cite{megaface,psychology,LFWTech,disguised_in_the_wild,openimagesv4}.
However, none of these focus on the specific challenge of face detection
under the disguise.

The main part of our dataset consists from OpenImagesV4~\cite{OpenImages2}.
Open Images is a dataset of $\sim$9M images that have been annotated with 
image-level labels, object bounding boxes and visual relationships.
The training set of V4 contains 14.6M bounding boxes for 600 object classes
on 1.74M images, making it the largest existing dataset with object
location annotations.
The boxes have been largely manually drawn by professional annotators to ensure
accuracy and consistency.
The images are very diverse and often contain complex scenes with
several objects (8.4 per image on average).
This also encourages structural image annotations, such as visual relationships.
Moreover, the dataset is annotated with image-level labels spanning thousands of classes.

Despite the amount of data in that dataset, we are interested only in 
class with faces.
The goal of our first neural network is to detect every face with or without
disguise on a frame.
Thus, the neural network to train requires not only faces from existing datasets,
but also faces with disguise and some counter-examples that are
not faces at all (Fig.~\ref{dataset}).

\begin{figure}[h!]
\subfloat[Disguised faces]{\includegraphics[width=.49\textwidth]{disguised.eps}}\hfill
\subfloat[Counter-examples]{\includegraphics[width=.49\textwidth]{counter.eps}}
\caption{Dataset additional data.} 
\label{dataset}
\end{figure}

Dataset for second neural network is more complicated than the first one.
The goal of the second network is to detect face parts on the output from
the first one (on a detected face).
The most common problem of face part detection is that all face parts are
detected together.
For example, in case when there is no nose or mouse, eyes could not be
detected.
To reduce the probability of such an outcome, we collected our dataset
with separate parts of a face:
\begin{itemize}
\item Eyes
\item Nose
\item Mouth
\end{itemize}

To collect dataset of separate parts we took images of disguised faces
and labeled visible parts on it.
Since sunglasses alone is not a disguise we decided to label them
alongside with face parts.
On figure~\ref{face_parts} different classes an outlined with different
color for clarity.
Red color for sunglasses is selected due to they are not part of a face
and we can not base our decision about face disguising only on sunglasses
presence.

\begin{figure}[h!]
\center
\includegraphics[width=.7\textwidth]{face_parts.eps}
\caption{Annotated face parts.} 
\label{face_parts}
\end{figure}

\section{Results}

To evaluate the proposed approach part of our dataset a custom
validation set was used.
This set consists of 370 different images of disguised faces,
undisguised faces and not faces at all.
For efficiency measurement we are using precision recall and
intersection over union (IoU) metrics.
Precision means the percentage of results which are relevant,
while recall refers to the percentage pf total relevant results
correctly classified by an algorithm.

On the validation dataset our algorithm showed the following results:
IoU: $\sim$60.28\%, Precision: $\sim$82.15\%, Recall: $\sim$83.42\%.
There is a simpler metric which takes into account both precision and
recall, and therefore, it could be more convenient to make a decision
about algorithm performance.
This metric is F1-score, which is simply the harmonic mean of precision
and recall (eq:~\ref{eq:f1}).
Thus, on that validation dataset, value of F1 score is about 82.78\%.
\begin{equation}
	\label{eq:f1}
	F_1 = 2*\frac{Precision * Recall}{Precision + Recall}
\end{equation}

Simple result of our algorithm is presented on figure~\ref{res}.
There are different detected face in real conditions.
Open faces are marked with green rectangle, while disguised are
with red one.

\begin{figure}[h!]
\center
\includegraphics[width=.7\textwidth]{results.eps}
\caption{Algorithm results.} 
\label{res}
\end{figure}

To show the dependence of neural network precision on the size of the face
WIDER FACE~\cite{wider_face} dataset was used.
WIDER FACE dataset is a face detection benchmark dataset, of which images are 
selected from the publicly available WIDER dataset.
It contains 32,203 images and label 393,703 annotated faces with a high degree
of variability in scale, pose and occlusion as depicted in the Fig.~\ref{wider}.
WIDER FACE dataset is organized based on 61 event classes. 
For each event class, randomly selected 40\%/10\%/50\% data as training,
validation and testing sets.

\begin{figure}[h!]
\center
\includegraphics[width=\textwidth]{wider_face}
\caption{Sample images from WIDER FACE dataset.} 
\label{wider}
\end{figure}

\begin{figure}[h!]
\center
\includegraphics[width=.9\textwidth]{face_prec}
\caption{The graph of dependence.} 
\label{face_prec}
\end{figure}

This dataset consists mainly of images where face area is small.
Despite of that fact, there are some images with bigger face area.
On the figure~\ref{face_prec} the graph of dependence of precision
on the size of face area is shown.
We can see, that the bigger image, the higher the precision of
detection.

\section{Conclusion}

In this paper we have proposed a mixed approach to detect disguised
faces on an image.
Our method uses two connected convolutional networks for face and
face parts detection.
Decision on whether the detected face is disguised or not our method
makes based on detected face parts.
The method was tested on different datasets and in real conditions.
It has been proven that disguised face detection precision reaches 83\%.
For testing in real conditions, a test bench was deployed that
detected disguised faces throughout the day.
This method does not require complex computations, which allows us to
run it on power-efficient embedded AI computing device - Jetson TX2.
Due to this fact it is easy to use our approach in security tasks,
such as video surveillance in public places to prevent crimes.

Further work can develop in different directions.
For example, the improvement of the training set allows to increase
the accuracy of the performance. 
It can allow to detect small faces or all faces from the crowd.
Dataset improvement can also allow us to detect disguised faces
in different conditions, such as low light, variety of accessories
and so on.

Another possible direction is to add a behavioral model.
This will allow us to predict behavior of a person and notify an
operator in case of possible danger.
To add behavioral model, we have to study different types of
behavioral in different situations.

\bibliographystyle{splncs04}
\bibliography{bibl}

\end{document}
